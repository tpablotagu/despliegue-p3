let tiposInmuebles = ["Piso", "Bungalow o adosado", "Ático", "Chalet", "Planta baja"];

module.exports = {
    tiposInmuebles: tiposInmuebles,
    standalone: true
}