const express = require("express");
const mongoose = require("mongoose");
const fileUpload = require("express-fileupload");
const bodyParser = require('body-parser');

const tipos = require("./routes/tipos");
const inmuebles = require("./routes/inmuebles");
const principal = require("./routes/index");

const TipoInmueble = require("./models/tipo");
const Inmueble = require("./models/inmueble");
const config = require("./config");

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let app = express();

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");
app.use("/public", express.static( __dirname + "/public"));
app.use(bodyParser.json());
app.use(fileUpload());

app.use("/", inmuebles);
app.use("/", tipos);
app.use("/", principal);

if(config.standalone){
    app.listen(5003);
} else {
    module.exports.app = app;
}
