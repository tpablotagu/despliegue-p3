const express = require('express');
let router = express.Router();

const Inmueble = require(`${__dirname}/../models/inmueble`);
const Tipo = require(`${__dirname}/../models/tipo`);

router.get("/inmuebles", (request, response) => {
    let precioMax = request.query.filtro_precio_input;
    let superficieMin = request.query.filtro_superficie_input;
    let habitacionesMin = request.query.filtro_habitaciones_input;
    let consulta = Inmueble.find();
    let filtros = {
        filtro_precio_anterior: undefined,
        filtro_superficie_anterior: undefined,
        filtro_habitaciones_anterior: undefined
    };

    if(typeof precioMax !== "undefined" && precioMax > 0) {
        consulta = consulta.where("precio").lt(precioMax);
        filtros.filtro_precio_anterior = precioMax;
    }
    if(typeof superficieMin !== "undefined" && superficieMin > 0) {
        consulta = consulta.where("superficie").gte(superficieMin);
        filtros.filtro_superficie_anterior = superficieMin;
    }
    if(typeof habitacionesMin !== "undefined" && habitacionesMin > 0 ) {
        consulta = consulta.where("habitaciones").gte(habitacionesMin);
        filtros.filtro_habitaciones_anterior = habitacionesMin;
    }
    
    consulta.populate("tipo")
    .then( inmuebles => {
        response.render("listar_inmuebles", { inmuebles: inmuebles, filtros: filtros });
    })
    .catch( error => {
        console.log("error obteniendo inmuebles: " + error);
        let mensaje = {
            estilo:"alert alert-danger",
            texto: "Se ha producido un error al obtener los inmuebles"
        };  
        response.redirect("/", {  mensaje: mensaje });
    });
});

router.get("/inmuebles/tipo/:idTipo", (request, response) => {
    Inmueble.find()
        .populate("tipo")
        .then( inmuebles => {
            let returning = inmuebles.filter( x => x.tipo._id == request.params.idTipo);
            response.render("listar_inmuebles", 
                { inmuebles: returning },
                (error, html) => {
                    console.log(error);
                });
        })
        .catch( error =>  {
            console.log(error);
        });
});

router.get("/inmuebles/:precio/:superficie/:habitaciones", (request, response) => {
    response.render("listar_inmuebles", 
        {},
        (error, html) => {
            console.log(error);
        });
});

router.get("/inmuebles/:id", (request, response) => {
    Inmueble.findById(request.params.id)
    .populate("tipo")
    .then(inmueble => {
        if(inmueble){
            response.render('ficha_inmueble', 
                { inmueble: inmueble },
                (error, html) => {
                    console.log(error);
                });
        } else {
            response.render('ficha_inmueble', 
                { mensaje: { estilo: "alert alert-danger", texto: "No existe el inmueble solicitado" }},
                (error, html) => {
                    console.log(error);
                });
        }
    })
    .catch( error => {
        Inmueble.find()
        .populate("tipo")
        .then( inmuebles => {
            response.render("listar_inmuebles", 
                { inmuebles: inmuebles, mensaje: { estilo: "alert alert-danger", texto: "No existe el inmueble solicitado" }},
                (error, html) => {
                    console.log(error);
                });
        })
    });    
});

router.post("/inmuebles", (request, response) => {
    console.log(request.body);
    let nuevoInmueble = request.body;
    let nombreFotoInmueble = "default";
    let extension = ".jpg";

    if(request.files !== null && typeof request.files.imagen_input !== "undefined") {
        extension = request.files.imagen_input.name.substring(request.files.imagen_input.name.lastIndexOf("."));
        nombreFotoInmueble = request.files.imagen_input.md5
    } 

    nombreFotoInmueble += extension;

    let inmueble = new Inmueble({ 
        descripcion: nuevoInmueble.descripcion_input,
        tipo: nuevoInmueble.tipo_input,
        habitaciones: nuevoInmueble.habitaciones_input,
        superficie: nuevoInmueble.superficie_input,
        precio: nuevoInmueble.precio_input,
        imagen: nombreFotoInmueble
    });

    inmueble.save()
        .then( x => {
            if(request.files !== null && typeof request.files.imagen_input !== "undefined"){
                request.files.imagen_input.mv("public/uploads/" + nombreFotoInmueble, (error) => {
                    if(error !== null) {
                        console.log(error);
                    }
                });
            }
            let mensaje = {
                estilo: "alert alert-success",
                texto: "Inmueble creado"
            }

            Inmueble.find()
                .populate("tipo")
                .then( inmuebles => {
                    response.render("listar_inmuebles", { inmuebles: inmuebles, mensaje: mensaje },
                        (error, html) => {
                            console.log(error);
                        });
                })
                .catch( error => {
                    console.log("error obteniendo inmuebles: " + error);
                    let mensaje = {
                        estilo:"alert alert-danger",
                        texto: "Se ha producido un error al obtener los inmuebles"
                    };
                    response.redirect("/", {  mensaje: mensaje });
                });
        })
        .catch( error => {
            console.log(error);
            let mensaje = {
                estilo: "alert alert-danger",
                texto: "Se ha producido un error al crear el inmueble"
            }
            response.render("nuevo_inmueble", 
                { mensaje: mensaje },
                (error, html) => {
                    console.log(error);
                }); 
        });
});

router.delete("/inmuebles/:id", (request, response) => {
    let x = request.params.id;
    Inmueble
        .findById(request.params.id)
        .remove()
        .then(result => {
            Inmueble.find()
            .populate("tipo")
            .then( inmuebles => {
                let mensaje = {
                    estilo: "alert alert-success",
                    texto: "Inmueble eliminado"
                };
                response.render("listar_inmuebles", 
                    { inmuebles: inmuebles, mensaje: mensaje },
                    (error, html) => {
                        console.log(error);
                    });
            })
            .catch( error => {
                console.log(error);
            });
        }).catch( error => {
            let mensaje = {
                estilo: "alert alert-danger",
                texto: "Se ha producido un error al eliminar el inmueble"
            };
            response.render("listar_inmuebles", 
                { inmuebles: inmuebles, mensaje: mensaje },
                (error, html) => {
                    console.log(error);
                });
        }) ;
});

module.exports = router;
