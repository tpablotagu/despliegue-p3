const express = require("express");

const Tipo = require(`${__dirname}/../models/tipo`);

let router = express.Router();

router.get("/", (request, response) => {
    response.render("index");
});

router.get("/nuevo_inmueble", (request, response) => {
    Tipo.find()
        .then( tiposInmueble => {
            response.render("nuevo_inmueble", { tiposInmueble: tiposInmueble });
        })
        .catch( error => {
            console.log("ERROR: " + error);
        });
});

module.exports = router;