
function limpiarFitros() {
    $("#filtro_precio_input").val("");
    $("#filtro_superficie_input").val("");
    $("#filtro_habitaciones_input").val("");
}

$(function() {
    $(".link-eliminar").click( function(){
        let x = $(this).data("id");
        var confirmacion = confirm("¿Estás seguro de que deseas eliminar el inmueble");
        if(confirmacion){
            eliminar(x);
        }
    });
  });

function eliminar(idInmueble){
    $.ajax({
        url:"/inmuebles/"+idInmueble,
        type:"DELETE",
        data: JSON.stringify({ id: idInmueble }),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(data) {
            console.log(data);
        },
        error: function(error){
            console.log(error);
        }
    });

}